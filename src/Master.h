#include "mbed.h"

class Master {

    private: 
    BufferedSerial serial;//BufferedSerial serial;    
    unsigned char function; // function to perform
    unsigned char slave_address; // ID of the slave device
    uint16_t reg_address; // (starting) address of the register to read from/write to
    uint16_t reg_value; // number of registers to read or value to write in the register
    uint16_t crc; 
    unsigned char size;
    unsigned char frame[64];
    uint16_t* registersM;
    uint16_t* registersP;
    uint16_t* valArray;

    void setFrame();
    void send();
    void receive();
    void setF_3_4_6();
    void setF16();    
    void receiveF3();
    void receiveF4();
    void receiveF6();
    void receiveF16();

    uint16_t calcCRC(unsigned char bufferSize);

    public:
    Master(PinName tx, PinName rx, long baud, uint16_t* registersM, uint16_t* registersP);    
    void setVars(unsigned char slave_address, unsigned char function, uint16_t reg_address, uint16_t reg_value, uint16_t* valArray = NULL);    
    void cycle(); 
    void frameProva();       
};