#include "Master.h"

// CONSTRUCTOR
Master::Master(PinName tx, PinName rx, long baud, uint16_t* registersM, uint16_t* registersP) : serial(tx, rx, baud) {
    serial.set_format(8, mbed::SerialBase::Even, 1);     
    this->registersM = registersM;
    this->registersP = registersP;
} 

// PUBLIC
void Master::setVars(unsigned char slave_address, unsigned char function, uint16_t reg_address, uint16_t reg_value, uint16_t* valArray) {
    this->function = function;
    this->slave_address = slave_address;
    this->reg_address = reg_address;
    this->reg_value = reg_value;  
    this->valArray = valArray;  
} 

void Master::cycle() {
    setFrame();
    send();
    receive();
}

// PRIVATE
void Master::send() {
    setFrame();    
    serial.write((void*)frame, size);   
    
    for (int i = 0; i < size; i++) {
        printf("%X ", frame[i]);
    }
    printf("\r\n");
}

void Master::receive() {
    Timer timeout;
    timeout.start();
    while(!serial.readable()) {
        thread_sleep_for(50); 
        if (timeout.read_ms() > 10000) {
            // printf("Nuova richiesta\r\n");
            timeout.stop();
            timeout.reset();            
            cycle();
            return;
        }          
    }
    timeout.stop();
    timeout.reset();
    thread_sleep_for(100);
    serial.read((void*)frame, 64);
    // printf("Ricevuto: \r\n");    
    for (int i = 0; i < 64; i++) {
        // printf("%d", frame[i]);
    }
    // printf("\r\n");
    
    if (frame[0] != slave_address) {
        return;
    }    
    switch(frame[1]) {
        case 3:
            receiveF3();
            return;
        case 4:
            receiveF4();
            return;
        case 6:
            receiveF6();
            return; 
    }
}

void Master::setFrame() {
    frame[0] = slave_address;
    frame[1] = function;
    switch (frame[1]) {
        case 3: case 4: case 6:
            setF_3_4_6();
            break;        
        case 16:
            setF16();
            break;
    }
}

void Master::setF_3_4_6() {
    size = 8;
    frame[2] = reg_address >> 8;
    frame[3] = reg_address & 0xFF;
    frame[4] = reg_value >> 8;
    frame[5] = reg_value & 0xFF;
    crc = calcCRC(size - 2);
    frame[6] = crc >> 8;
    frame[7] = crc & 0xFF;
}

void Master::setF16() {
    frame[2] = reg_address >> 8;
    frame[3] = reg_address & 0xFF;
    frame[4] = reg_value >> 8;
    frame[5] = reg_value & 0xFF;
    frame[6] = reg_value * 2;    
    for (int i = 0, k = 7; i < reg_value; i++, k+=2) {
        frame[k] = valArray[i] >> 8;
        frame[k + 1] = valArray[i] &0xFF; 
    }
    size = 9 + frame[6];
    crc = calcCRC(size - 2);
    frame[size - 2] = crc >> 8;
    frame[size - 1] = crc & 0xFF;
}

void Master::receiveF3() {    
    size = 5 + frame[2];
    uint16_t recCRC = (frame[size-2] << 8) | frame[size-1];
    if (calcCRC(size-2) != recCRC) {
        // printf("Errore CRC F3!\r\n");
        return;
    }
    
    int k = 0;
    for (int i = 3; i < size-2; i+=2, k++) {
        registersM[k] = (frame[i] << 8) | frame[i+1];
        // printf("%d\r\n", registersM[k]);
    }     
}

void Master::receiveF4() {     
    size = 5 + frame[2];
    uint16_t recCRC = (frame[size-2] << 8) | frame[size-1];
    if (calcCRC(size-2) != recCRC) {
        // printf("Errore CRC F4!\r\n");
        return;
    }
    
    int k = 0;
    for (int i = 3; i < size-2; i+=2, k++) {
        registersP[k] = (frame[i] << 8) | frame[i+1];
        // printf("%d\r\n", registersP[k]);
    }         
}

void Master::receiveF6() {
    uint16_t recCRC = (frame[6] << 8) | frame[7];
    if (recCRC != calcCRC(6)) {
        // printf("Errore CRC F6!\r\n");
        return;
    } 
    /*
    printf("Function: %d\n", frame[0]);
    printf("Slave Address: %d\n", frame[1]);
    printf("Reg Address: %d\n", (frame[2] << 8) | frame[3]);
    printf("Reg Value: %d\n", (frame[4] << 8) | frame[5]);
    printf("CRC: %d\n\n", (frame[6] << 8) | frame[7]);
    */
}

uint16_t Master::calcCRC(unsigned char bufferSize) {
    uint16_t temp, flag;
    temp = 0xFFFF;
    for (unsigned char i = 0; i < bufferSize; i++) {
        temp = temp ^ frame[i];
        for (unsigned char j = 1; j <= 8; j++) {
            flag = temp & 0x0001;
            temp >>= 1;
            if (flag)
                temp ^= 0xA001;
        }
    }
    uint16_t temp2, temp3;
    temp2 = temp >> 8;
    temp3 = temp & 0xFF;
    temp = (temp3 << 8) | temp2; 
    return temp;
}